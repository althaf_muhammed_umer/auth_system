from rest_framework import serializers
from .models import CustomUser

class UserSerializer(serializers.ModelSerializer):
    
    password = serializers.CharField(write_only=True, required=True)
    
    class Meta:
        model = CustomUser
        fields = ('id', 'username', 'email', 'password')
        
    
    def create(self, validated_data):
        email = validated_data["email"]
        username = email.split("@")[0]
        
        user = CustomUser.objects.create(
            email=email,
            username=username,
        )
        
        user.set_password(validated_data['password'])
        
        user.save()
        
        return user