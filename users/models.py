from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
# Create your models here.

class CustomUserManager(BaseUserManager):
    
    def create_user(self, email, password=None):
        if not email:
            raise ValueError("Users must have an email address")
        
        user_obj = self.model(
            
            email =self.normalize_email(email),
            
        )
        
        user_obj.set_password(password)
        user_obj.save(using=self._db)
        return user_obj
    
    def create_superuser(self, email, password=None):
        user_obj = self.create_user(
            email = self.normalize_email(email),
            password=password,
            
        )
        
        user_obj.is_superuser = True
        user_obj.is_staff = True
        user_obj.is_active = True
        user_obj.save(using=self._db)
        
        return user_obj
    
    

class CustomUser(AbstractBaseUser):
    username = models.CharField(max_length=80, null=True)
    email = models.CharField(max_length=50, unique=True)

    # required fields
    is_admin = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)

    
    objects = CustomUserManager()
    
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = [username]
    
    def __str__(self):
        return self.email
    
    
    def has_perm(self, perm, obj=None):
        return self.is_superuser

    def has_module_perms(self, app_label):
        return self.is_superuser

    def get_all_permissions(user=None):
        if user.is_superuser:
            return set()
