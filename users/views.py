from django.shortcuts import render
from rest_framework.decorators import api_view
from . serializers import UserSerializer
from . models import CustomUser
from rest_framework.response import Response
from rest_framework import status


@api_view(['POST'])
def register(request):
    data = request.data
    serializer = UserSerializer(data=data)
    
    if serializer.is_valid():
        
        serializer.save()
        
        
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    
    else:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        
        
        
        
    
    
